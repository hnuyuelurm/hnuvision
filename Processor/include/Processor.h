#ifndef PROCESSOR_H
#define PROCESSOR_H

#include "TSolver.hpp"
#include "tracker.hpp"
#include "armor.hpp"
#include "DataType.hpp"
#include <vector>
#include <chrono>
#include <memory>

namespace hnurm
{
    class Processor
    {
        using Us = std::chrono::microseconds;

    public:
        Processor() = default;

        /**
         * @brief Processor中维护了一个Tracker和一个TSolver，参数均在此初始化
         *
         * @param cfg_node 包含tracker_config_node与tracker_config_node
         */
        Processor(const cv::FileNode &cfg_node);

        /* 传入当前云台的pitch和yaw */
        /**
         *
         * @param armors_msg [in]  armors
         * @param pitch pitch
         * @param yaw  yaw
         * @param target_msg [out] target
         */
        void ProcessArmor(std::vector<Armor> &armors_msg, VisionRecvData &recv_data, TargetInfo &target_msg);

        std::unique_ptr<Tracker> tracker;
        std::unique_ptr<TSolver> tsolver;

    private:

        double s2qxyz_,s2qyaw_,s2qr_;

        double r_xyz_factor, r_yaw;

        std::chrono::steady_clock::time_point time_elapsed_;
        double dt_; // time interval between two frames
    };

}
#endif // PROCESSOR_H