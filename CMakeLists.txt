cmake_minimum_required(VERSION 3.16)
project(hnuvision)
set(CMAKE_CXX_STANDARD 17)
set(CMAKE_BUILD_TYPE release)

#ADD_COMPILE_DEFINITIONS(DEBUG_PROCESS )
#add_compile_definitions(DEBUG_PARAMETERS DEBUG_SHOW)

find_package(OpenCV REQUIRED)
find_package(Eigen3 REQUIRED)

if(${CMAKE_SYSTEM} MATCHES "Linux")
    file(GLOB SDK_PATH ${CMAKE_CURRENT_SOURCE_DIR}/IndustrialCamera/lib/linux/*)
elseif (${CMAKE_SYSTEM} MATCHES "Darwin")
    file(GLOB SDK_PATH ${CMAKE_CURRENT_SOURCE_DIR}/IndustrialCamera/lib/darwin/*)
endif ()

include_directories(
        ${OpenCV_INCLUDE_DIRS}
        ${CMAKE_CURRENT_SOURCE_DIR}
        ${CMAKE_CURRENT_BINARY_DIR}
        ${EIGEN3_INCLUDE_DIRS}
)

include_directories(
        ArmorDetectorT/include
        Communication/CRC/include
        Communication/Serial/include
        Communication/Protocol/include
        Compensator/include
        Data/
        IndustrialCamera/include
        ThreadManager/include
        Logging/include
        Processor/include
        utils/Angles
)

file(GLOB_RECURSE source
        ArmorDetectorT/src/*
        ArmorDetectorT/include/*
        Communication/CRC/src/*
        Communication/Serial/src/*
        Communication/Protocol/src/*
        Compensator/src/*
        Processor/src/*
        IndustrialCamera/src/*
        ThreadManager/src/*
        Logging/src/*
        )

add_executable(${PROJECT_NAME}
        main.cpp ${source})

target_link_libraries(${PROJECT_NAME}
        ${OpenCV_LIBS}
        ${Eigen3_LIBRARIES}
        ${SDK_PATH}
        -lpthread -lm
        )